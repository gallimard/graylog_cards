import * as React from "react";
import './App.css';
import { Route, Routes, BrowserRouter, Navigate } from "react-router-dom";
import CardsList from './pages/cards/CardsList';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<CardsList />} />
        <Route path="*" element={<Navigate to={"/" + window.location.search}  />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
