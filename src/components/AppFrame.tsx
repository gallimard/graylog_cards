import * as React from "react";
import { styled } from "@mui/material/styles";

interface AppFrameProps {
    children : JSX.Element
    appBar : JSX.Element
}

export const AppFrame = ({ appBar, children }: AppFrameProps) => {

    const Root = styled('div')({
        display: "flex",
        flexDirection: "row",
        position: "absolute",
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        overflowY: "hidden",
        overflowX: "hidden",
        backgroundColor: "#efefef"
    });

    const Main = styled('main')({
        flex: 1,
        display:"flex",
        flexDirection:"column",
        minWidth: 0
    });

    const Content = styled('div')({
        display: "flex",
        flexDirection: "column",
        position: "relative",
        flexGrow: 1,
        overflowY: "auto",
        WebkitOverflowScrolling: "touch",
        padding: 32
    });

    return (
        <Root>
            <Main>
                {appBar}
                <Content>
                    {children}
                </Content>
            </Main>
        </Root>
    )
   
};