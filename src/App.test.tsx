import React from 'react';
import { act, fireEvent, render, screen } from '@testing-library/react';
import App from './App';

test('renders cards app', () => {
  render(<App />);
  expect(screen.getByText(/Cards/i)).toBeInTheDocument();
});

test('filter dialog opens after filter button click', async () => {
  act(()=>{
    render(<App />)
  })
  
  expect(screen.queryByTestId('filter-dlg')).toBeNull()

  fireEvent.click(screen.getByTestId('filter-btn'))

  const filterDlg = screen.getByTestId('filter-dlg')

  expect(filterDlg).toBeInTheDocument();

  expect(filterDlg).toHaveTextContent('Filter')
  expect(filterDlg).toHaveTextContent('Name')
  expect(filterDlg).toHaveTextContent('Colors')
  expect(filterDlg).toHaveTextContent('Reset')
  expect(filterDlg).toHaveTextContent('Save')
})