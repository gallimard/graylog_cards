import * as React from 'react';
import * as Magic from "mtgsdk-ts";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';

interface CardsListItemProps {
    card: Magic.Card
}

export const CardsListItem = ({ card }: CardsListItemProps) => {
  
  const [openDetailsDlg, setOpenDetailsDlg] = React.useState<boolean>(false)

  const ImageContainer = styled('div')({
    padding: 32, 
    justifyContent: "center", 
    display: "flex"
  });

  return (
    <Card sx={{ width: "100%" }}>
      <CardMedia
        component="img"
        height="320"
        image={card.imageUrl}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div" noWrap>
          {card.name}
        </Typography>
        <Typography gutterBottom variant="body2" color="text.secondary" noWrap>
          {card.type}
        </Typography>
        <Typography gutterBottom variant="caption" color="text.secondary" noWrap>
          {card.artist}
        </Typography>
      </CardContent>
      <CardActions style={{ justifyContent: "right" }}>
        <Button size="small" color="secondary" onClick={() => setOpenDetailsDlg(true)}>details</Button>
      </CardActions>
      <Dialog onClose={() => setOpenDetailsDlg(false)} open={openDetailsDlg}>
        <DialogTitle>{card.name}</DialogTitle>
        <DialogContent>
          <ImageContainer>
            <img src={card.imageUrl} alt="" />
          </ImageContainer>
          {Object.entries(card).map(entry => (
              <Grid 
                  container
                  wrap="nowrap"
                  key={entry[0]}
              >
                  <Grid item xs={6}>
                    <Typography><strong>{entry[0]}</strong>:</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography style={{ wordBreak: "break-all" }} gutterBottom>{JSON.stringify(entry[1])}</Typography>
                  </Grid>
              </Grid>
          ))}
        </DialogContent>
      </Dialog>
    </Card>
  );
}