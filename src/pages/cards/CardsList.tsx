import * as React from "react";

import Grid from "@mui/material/Grid";
import LinearProgress from "@mui/material/LinearProgress";

import { AppFrame } from "../../components/AppFrame";
import { CardsAppBar } from "./CardsAppBar";
import { CardsListItem } from "./CardsListItem";
import { useCards } from "./hooks/useCards";

const CardsList = () => {
    
    const { cards, loading } = useCards()

    return (
        <AppFrame appBar={<CardsAppBar cardsLength={cards.length} />}>
            {loading 
            ?  <LinearProgress color="secondary" />
            : (
                <Grid 
                    container 
                    spacing={{ xs: 3 }} 
                    justifyContent="center" 
                    alignItems="center"
                >
                    {cards.map(card => (
                        <Grid
                            item 
                            container 
                            xs={12} sm={6} md={4} lg={3} 
                            justifyContent="center" 
                            alignItems="center" 
                            key={card.id}
                        >
                            <CardsListItem card={card} />
                        </Grid>
                    ))}
                </Grid>
            )}
        </AppFrame>
    )
   
};

export default CardsList;