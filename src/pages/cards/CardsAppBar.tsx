import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import FilterIcon from '@mui/icons-material/FilterList';
import ColorIcon from '@mui/icons-material/Square';
import IconButton from '@mui/material/IconButton';
import TablePagination from '@mui/material/TablePagination';
import { createSearchParams, useSearchParams } from 'react-router-dom';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import OutlinedInput from '@mui/material/OutlinedInput';
import MenuItem from '@mui/material/MenuItem';
import Checkbox from '@mui/material/Checkbox';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';
import { ROWS_PER_PAGE } from './hooks/useCards';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';

interface CardsAppBarProps {
  cardsLength?: number
}

export const CardsAppBar = ({ cardsLength=ROWS_PER_PAGE }: CardsAppBarProps) => {

  const [searchParams, setSearchParams] = useSearchParams();
  
  const [openFilterDlg, setOpenFilterDlg] = React.useState<boolean>(false)
  const [name, setName] = React.useState<string>(searchParams.get("name") || "")
  const [colors, setColors] = React.useState<string[]>(searchParams.getAll("colors") ||[])
  const [colorsOP, setColorsOP] = React.useState<"and"|"or">(searchParams.get("colorsOP") as "and"|"or" || "or")

  const currPage = Number(searchParams.get("page") || 0);

  const handleReset = () => {
    setName("")
    setColors([])
    setColorsOP("or")
  }

  const handleQueryParamChanges = (page: string = String(currPage)) => {
    setSearchParams(
      createSearchParams({
        page,
        name, 
        colors, 
        colorsOP: (colors.length > 1) ? colorsOP : "",
      })
    );
  }

  return (
      <AppBar position="static" elevation={0} color="secondary">
        <Toolbar>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ display: { xs: 'none', sm: 'block' } }}
          >
            Cards
          </Typography>
          <div style={{ flexGrow: 1 }} />
          <IconButton data-testid="filter-btn" color="inherit" onClick={() => setOpenFilterDlg(true)}>
            <FilterIcon />
          </IconButton>
          <TablePagination
            sx={{ color: 'inherit', textAlign: "right" }}
            size="small"
            component="div"
            count={(cardsLength < ROWS_PER_PAGE) ? (currPage + 1) * ROWS_PER_PAGE : -1}
            page={currPage}
            rowsPerPageOptions={[]}
            rowsPerPage={ROWS_PER_PAGE}
            onPageChange={(e, nextPage) => {
              handleQueryParamChanges(String(nextPage || 0));
            }}
          />
        </Toolbar>
        <Dialog data-testid="filter-dlg" onClose={() => setOpenFilterDlg(false)} open={openFilterDlg}>
          <DialogTitle>Filter</DialogTitle>
          <DialogContent style={{ padding: 32, width: 240 }}>
            <TextField
              fullWidth
              label="Name"
              value={name}
              onChange={e => setName(e.target.value)}
              style={{ marginBottom: 16 }}
              color="secondary"
            />
            <FormControl style={{ marginBottom: 16 }} color="secondary" fullWidth>
              <InputLabel id="select_colors_label">Colors</InputLabel>
              <Select
                labelId="select_colors_label"
                id="select_colors"
                multiple
                value={colors}
                onChange={e => setColors(typeof e.target.value === 'string' ? e.target.value.split(',') : e.target.value)}
                input={<OutlinedInput label="Colors" />}
                renderValue={(selected) => selected.join(', ')}
              >
                {["red", "blue", "black", "white", "green"].map((color) => (
                  <MenuItem key={color} value={color}>
                    <Checkbox checked={colors.indexOf(color) > -1} />
                    <ListItemText primary={color} />
                    <ListItemIcon>
                      <ColorIcon style={{ color: color }} />
                    </ListItemIcon>
                  </MenuItem>
                ))}
              </Select>
              {(colors.length > 1) && 
                <RadioGroup
                  row
                  style={{ marginTop: 8 }}
                  value={colorsOP}
                  onChange={e => setColorsOP(e.target.value as "or"|"and")}
                >
                  <FormControlLabel value="or" control={<Radio color="secondary" />} label="or" />
                  <FormControlLabel value="and" control={<Radio color="secondary" />} label="and" />
                </RadioGroup>
              }
            </FormControl>
          </DialogContent>
          <DialogActions>
            <Button 
              onClick={handleReset}
              color="secondary"
            >
              Reset
            </Button>
            <Button 
              variant="contained"
              color="secondary"
              onClick={()=>{
                handleQueryParamChanges();
                setOpenFilterDlg(false)
              }}
            >
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </AppBar>
  );
}
