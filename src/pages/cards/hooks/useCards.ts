import { useEffect, useRef, useState } from "react";
import * as Magic from "mtgsdk-ts";
import { useSearchParams } from "react-router-dom";

const isEqual = require("lodash/isEqual");

export const ROWS_PER_PAGE = 12

export const useCards = (
    filter: Magic.CardFilter = {}
): { cards: Magic.Card[], loading: boolean } => {
    
    const [searchParams] = useSearchParams();
    const [cards, setCards] = useState<Magic.Card[]>([])
    const [loading, setLoading] = useState<boolean>(true)
    const prevFilter = useRef<Magic.CardFilter|null>(null)

    const currFilter: Magic.CardFilter = {
        contains: "imageUrl",
        page: Number(searchParams.get("page") || 0) + 1,
        pageSize: ROWS_PER_PAGE,
        name: searchParams.get("name") || undefined,
        colors: (searchParams.get("colorsOP") === "and") ? searchParams.getAll("colors").join(",") : searchParams.getAll("colors").join("|"),
        ...filter
    }

    const shouldRefetch = !isEqual(currFilter, prevFilter.current);

    useEffect(()=>{
        if(shouldRefetch){
            prevFilter.current = currFilter
            setLoading(true)
            Magic.Cards.where(currFilter)
            .then((data)=>{
                setCards(data)
            }).catch((e)=>{
                // display error
                console.log(e)
            }).finally(()=>{
                setLoading(false)
            })
        }
    }, [shouldRefetch, prevFilter.current, currFilter])

    return { cards, loading }
}